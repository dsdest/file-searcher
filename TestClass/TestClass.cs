using System;
using System.Collections.Generic;
using Xunit;

// ��� ����������� ������ ������ ������ DivideFullName
namespace TestClass
{
    public class DivideFullNameTestClass
    {
        [Fact]
        public void NameOnly_Test()
        {
            // setup
            string init = "C:\\Program Files (x86)\\Adobe\\Adobe Creative Cloud\\CoreSyncPlugins\\DesignLibraryPlugin\\DesignLibraryPlugin.pimx";
            string expected = "DesignLibraryPlugin.pimx";
            // act
            string actual = FlieSearching.DivideFullName.NameOnly(init);
            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DirOnly_Test()
        {
            // setup
            string init = "C:\\Program Files (x86)\\Adobe\\Adobe Creative Cloud\\CoreSyncPlugins\\DesignLibraryPlugin\\DesignLibraryPlugin.pimx";
            string expected = "C:\\Program Files (x86)\\Adobe\\Adobe Creative Cloud\\CoreSyncPlugins\\DesignLibraryPlugin\\";
            // act
            string actual = FlieSearching.DivideFullName.DirOnly(init);
            // assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void PathToQueue_Test()
        {
            // setup
            string init = "C:\\Program Files (x86)\\Adobe\\DesignLibraryPlugin.pimx";
            Queue<string> expected = new Queue<string>();
            expected.Enqueue("C:\\");
            expected.Enqueue("Program Files (x86)\\");
            expected.Enqueue("Adobe\\");
            expected.Enqueue("DesignLibraryPlugin.pimx");

            // act
            Queue<string> actual = new Queue<string>();
            FlieSearching.DivideFullName.PathToQueue(actual, init);
            
            // assert
            Assert.Equal(expected, actual);
        }
        
    }
}
