﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace FlieSearching
{
    class searcher
    {
        public event Action<string> getFile;            // событие для получения названия файла. передаёт путь к файлу
        public event Action<string> fileMatch;          // событие проверки полученного файла на соответствие
        public event Action<Exception> exHandler;       // событие для обработки всяких исключений
        public event Action<int> amountOfFiles;         // событие для получения числа обработанных файлов
        public event Action<Exception> searchComplete;   // событие при завершении поиска

        private string filePath;
        private string fileName;
        private string fileContains;
        public searcher(string filePath, string fileName, string fileContains)
        {
            this.filePath = filePath;
            this.fileName = fileName;
            this.fileContains = fileContains;

        }

        public void search()
        {
            try
            {
                int i = 0;
                foreach (var file in Directory.EnumerateFiles(filePath, "*.*", SearchOption.AllDirectories))
                {
                    getFile(file);
                    fileMatch(file);
                    amountOfFiles(++i);
                    Thread.Sleep(0);
                }
                // вызываем исключение, чтобы прошло уведомление о завершении поиска
                Exception e = new Exception();
                searchComplete(e);      
            }
            catch(ThreadAbortException ex)
            {
                searchComplete(ex);
            }

            catch(Exception ex)
            {
                exHandler(ex);
            }
                
        }

    }
}
