﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlieSearching
{
    public static class DivideFullName     // работа с именем файла, выделение нужной нам информации из его имени
    {

        // получить только название файла без его директории
        public static string NameOnly(string s)        
        {

            int i = s.IndexOf('\\');
            return (i == -1 ? s : NameOnly(s.Substring(i + 1)));

        }

        // получить только директорию
        public static string DirOnly(string s)
        {
            int i = s.IndexOf(NameOnly(s),0);
            s = s.Substring(0, i);
            return s;
        }

        
        // получить путь к файлу в виде очереди
        public static void PathToQueue(Queue<string> ret, string fPath)
        {
            int i = fPath.IndexOf('\\');
            if (i != -1)
            {
                ret.Enqueue(fPath.Substring(0, i + 1));
                fPath = fPath.Substring(i + 1);
                PathToQueue(ret, fPath);
            }
            else ret.Enqueue(fPath);

        }

    }
}
