﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FlieSearching
{

    // статик класс, превращающий очередь в ноду и добавляющий сформированную ноду в объект TreeView
    public static class TreeViewHandler
    {
        public static TreeNode generateNodes(Queue<string> path)
        {
            // сформировать узел из элемента очереди
            var dirNode = new TreeNode(path.Dequeue());
            // поскольку в очереди теперь стало на элемент меньше
            // но всё ещё остались директории
            // повторяем, пока их не останется
            for (int i = 0; i < path.Count; i++)
                dirNode.Nodes.Add(generateNodes(path));
            // заполненную ноду возвращаем
            return dirNode;
        }

        public static void addElement(TreeView tree, Queue<string> path)
        {
            tree.BeginUpdate();
            tree.Nodes.Add(generateNodes(path));    // просто добавляем заполненную другим методом ноду в указанный объект типа TreeView
            tree.EndUpdate();
        }
    }
}
