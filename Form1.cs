﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace FlieSearching
{
    public partial class Form1 : Form
    {
        string initPath;                    // папка, с которой начать поиск
        string fileName;                    // шаблон имени
        string fileText;                    // текст в файле
        DateTime beginTime;                 // для отсчета времени
        Thread search;                      //поисковый отдельный поток
        int fileCounter = 0;                // число найденых файлов, будет отображаться в TextBox fileFoundAmount
        string fileDataPath = "pr_d.fs";    // файл, в котором содержится информация о вводе информации в поля в предыдущем сеансе
        bool ClosedByUser=false;            // вернёт TRUE при нажатии кнопочки закрытия формы

        public Form1()
        {
            InitializeComponent();
        }

        private void StartDirButton_Click(object sender, EventArgs e)
        {
            // получаем папку, с которой начинается поиск
            if (StartDirectoryBrowser.ShowDialog() == DialogResult.OK)
            {
                StartDirTextBox.Text = StartDirectoryBrowser.SelectedPath;

                // блокируем ненужные кнопки и разблокочиваем нужные, чтоб пользователь не крашнул программу раньше времени
                PauseSearchButton.Enabled = false;
                if (SearchButton.Enabled == false) SearchButton.Enabled = true;
            }
        }

        // следующие 2 события носят чисто декоративный характер

        // при наведении на текстбокс курсора он очищается, если в нем находится текст по умолчанию
        private void FileNameTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (FileNameTextBox.Text == "Имя файла") FileNameTextBox.Text = "";
        }
        // при отведении курсора с текстбокса вставляем текст по умолчанию, если в боксе пусто
        private void FileNameTextBox_MouseLeave(object sender, EventArgs e)
        {
            if (FileNameTextBox.Text == "") FileNameTextBox.Text = "Имя файла";
        }

        // кнопка начала поиска
        private void SearchButton_Click(object sender, EventArgs e)
        {
            // меняем значения и доступность кнопочек
            PauseSearchButton.Text = "Приостановить поиск";
            StartDirButton.Enabled = false;
            SearchButton.Enabled = false;
            

            // заполняем глобальные переменные формы введёнными начальными условиями
            this.initPath = StartDirTextBox.Text;
            this.fileName = FileNameTextBox.Text;
            this.fileText = richTextBox1.Text;
            this.fileCounter = 0;

            // получаем время начала поиска, чтоб потом вычитать его из текущего времени
            this.beginTime = DateTime.Now;

            // создаём s класса search
            // передаём в него начальную папку, шаблон имени файла и содержащийся в файле текст
            var s = new searcher(initPath, fileName, fileText);

            // подписываем объект s класса searcher на события
            s.getFile += S_getFile;                 // просто перебор файлов
            s.fileMatch += S_fileMatch;             // запуск при совпадении (даже частичном) имени файла с найденым
            s.exHandler += S_exHandler;             // исключение для пустого начального пути
            s.amountOfFiles += S_amountOfFiles;     // вызывается чтоб прописать количество уже посмотренных файлов в текстбоксе FilesAmount
            s.searchComplete += S_searchComplete;   // вызывается при завершении поиска

            // запускаем поток поиска
            search = new Thread(s.search);
            search.Start();

            // чистим дерево и обнуляем текстбокс с количеством найденых по шаблону файлов
            fileFoundAmount.Text = "0";
            treeView1.Nodes.Clear();

        }

        // событие запускается при завершении поиска
        private void S_searchComplete(Exception obj)
        {
            // проверка нужна чтобы код сработал только при завершении поиска
            // чтобы окно поиск завершён не вылетало при закрытии формы
            if(!ClosedByUser)
            {
            MessageBox.Show("Поиск завершён", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Action action = () =>
            {
                PauseSearchButton.Text = "Приостановить поиск";
                PauseSearchButton.Enabled = false;
                SearchButton.Enabled = true;
                StartDirButton.Enabled = true;
            };

            Invoke(action);

            }

        }

        // событие вызывается при переходе s.search() к новому файлу
        private void S_amountOfFiles(int obj)
        {
            Action action = () =>
              {
                  FilesAmount.Text = obj.ToString();        // obj - это переменная i метода searcher.search()
              };
            Invoke(action);

        }

        // чтоб не крашнулось при попытке передать пустой путь
        private void S_exHandler(Exception obj)
        {
            MessageBox.Show(obj.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Action action = () => { SearchButton.Enabled = true; StartDirButton.Enabled = true; };
            Invoke(action);
        }


        // событие проверяет совпадение имени и текста внутри файла с заданными критериями
        private void S_fileMatch(string obj)
        {
            Action action = () =>
            {
                if (obj.Contains(fileName))
                {
                    using (StreamReader reader = new StreamReader(obj))
                    {
                        List<string> s = new List<string>();
                        while (!reader.EndOfStream)
                        {
                            s.Add(reader.ReadLine());
                        }
                        foreach (string sch in s)
                            if (sch.Contains(fileText))                                         // найден файл, который полностью отвечает критериям поиска
                            {
                                fileFoundAmount.Text = Convert.ToString(++fileCounter);         // увеличиваем счётчик найденных файлов на 1
                                Queue<string> q = new Queue<string>();
                                DivideFullName.PathToQueue(q, obj);                             // делаем очередь из пути файла
                                TreeViewHandler.addElement(treeView1,q);                        // и методом статик класса превращаем очередь в ноду для treeView1
                            }
                    }
                }
            };
            Invoke(action);
        }

        

        // вызов при получении нового файла
        private void S_getFile(string obj)
        {
            Action action = () =>
            {
                FileTextBox.Text = DivideFullName.NameOnly(obj);                        // в текстбокс пишем только имя файла, который сейчас в работе
                TimePassed.Text = Convert.ToString(DateTime.Now - this.beginTime);      // чтоб знать, сколько времени прошло
                PauseSearchButton.Enabled = true;
            };
            Invoke(action);
        }

        // кнопка PauseSearchButton тут играет как роль приостановки, так и роль возобновления поиска, в зависимости от текста в ней
        private void PauseSearchButton_Click(object sender, EventArgs e)
        {
            // если нажимается приостановка
            if (PauseSearchButton.Text == "Приостановить поиск")
            {
                PauseSearchButton.Text = "Возобновить поиск";
                StartDirButton.Enabled = true;
                search.Suspend();
            }
            // если нажимается возобновление
            else
            {
                PauseSearchButton.Text = "Приостановить поиск";
                StartDirButton.Enabled = false;
                search.Resume();
            }
        }

        // подгружаем информацию с предыдущего сеанса, если соответствующий файл существует
        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(fileDataPath))
            {
                using (StreamReader s = new StreamReader(fileDataPath))
                {
                    StartDirTextBox.Text = s.ReadLine();
                    FileNameTextBox.Text = s.ReadLine();
                    richTextBox1.Text = s.ReadLine();
                }
            }
        }

        // при необходимости создаём и загружаем в файл информацию о предыдущем сеансе
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClosedByUser = true;
            // проверяем, не пустой ли поток
            if ( search!= null && search.IsAlive)
            {
                search.Resume();
                search.Abort();
            }
            using (StreamWriter s = new StreamWriter(fileDataPath))
            {
                    s.WriteLine(StartDirTextBox.Text);
                    s.WriteLine(FileNameTextBox.Text);
                    s.WriteLine(richTextBox1.Text);
            }
        }
    }
}
