﻿namespace FlieSearching
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartDirButton = new System.Windows.Forms.Button();
            this.StartDirTextBox = new System.Windows.Forms.TextBox();
            this.FileNameTextBox = new System.Windows.Forms.TextBox();
            this.StartDirectoryBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SearchButton = new System.Windows.Forms.Button();
            this.FileTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.PauseSearchButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.FilesAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TimePassed = new System.Windows.Forms.TextBox();
            this.fileFoundAmount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StartDirButton
            // 
            this.StartDirButton.Location = new System.Drawing.Point(424, 6);
            this.StartDirButton.Name = "StartDirButton";
            this.StartDirButton.Size = new System.Drawing.Size(35, 20);
            this.StartDirButton.TabIndex = 0;
            this.StartDirButton.Text = "...";
            this.StartDirButton.UseVisualStyleBackColor = true;
            this.StartDirButton.Click += new System.EventHandler(this.StartDirButton_Click);
            // 
            // StartDirTextBox
            // 
            this.StartDirTextBox.Location = new System.Drawing.Point(224, 6);
            this.StartDirTextBox.Name = "StartDirTextBox";
            this.StartDirTextBox.Size = new System.Drawing.Size(194, 20);
            this.StartDirTextBox.TabIndex = 1;
            // 
            // FileNameTextBox
            // 
            this.FileNameTextBox.ForeColor = System.Drawing.SystemColors.MenuText;
            this.FileNameTextBox.Location = new System.Drawing.Point(224, 32);
            this.FileNameTextBox.Name = "FileNameTextBox";
            this.FileNameTextBox.Size = new System.Drawing.Size(235, 20);
            this.FileNameTextBox.TabIndex = 2;
            this.FileNameTextBox.Text = "Имя файла";
            this.FileNameTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FileNameTextBox_MouseClick);
            this.FileNameTextBox.MouseLeave += new System.EventHandler(this.FileNameTextBox_MouseLeave);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(9, 79);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(450, 92);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Текст в файле";
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(9, 177);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(450, 35);
            this.SearchButton.TabIndex = 5;
            this.SearchButton.Text = "Искать файл";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // FileTextBox
            // 
            this.FileTextBox.Location = new System.Drawing.Point(157, 215);
            this.FileTextBox.Name = "FileTextBox";
            this.FileTextBox.ReadOnly = true;
            this.FileTextBox.Size = new System.Drawing.Size(302, 20);
            this.FileTextBox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Укажите начальную директорию поиска";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Укажите название файла";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Сейчас обрабатывается...";
            // 
            // treeView1
            // 
            this.treeView1.LineColor = System.Drawing.Color.Gray;
            this.treeView1.Location = new System.Drawing.Point(490, 6);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(982, 381);
            this.treeView1.TabIndex = 11;
            // 
            // PauseSearchButton
            // 
            this.PauseSearchButton.Enabled = false;
            this.PauseSearchButton.Location = new System.Drawing.Point(9, 329);
            this.PauseSearchButton.Name = "PauseSearchButton";
            this.PauseSearchButton.Size = new System.Drawing.Size(450, 58);
            this.PauseSearchButton.TabIndex = 12;
            this.PauseSearchButton.Text = "Приостановить поиск";
            this.PauseSearchButton.UseVisualStyleBackColor = true;
            this.PauseSearchButton.Click += new System.EventHandler(this.PauseSearchButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 244);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Файлов обработано";
            // 
            // FilesAmount
            // 
            this.FilesAmount.Location = new System.Drawing.Point(157, 241);
            this.FilesAmount.Name = "FilesAmount";
            this.FilesAmount.ReadOnly = true;
            this.FilesAmount.Size = new System.Drawing.Size(302, 20);
            this.FilesAmount.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 270);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Времени прошло";
            // 
            // TimePassed
            // 
            this.TimePassed.Location = new System.Drawing.Point(157, 267);
            this.TimePassed.Name = "TimePassed";
            this.TimePassed.ReadOnly = true;
            this.TimePassed.Size = new System.Drawing.Size(302, 20);
            this.TimePassed.TabIndex = 15;
            // 
            // fileFoundAmount
            // 
            this.fileFoundAmount.Location = new System.Drawing.Point(187, 293);
            this.fileFoundAmount.Name = "fileFoundAmount";
            this.fileFoundAmount.ReadOnly = true;
            this.fileFoundAmount.Size = new System.Drawing.Size(272, 20);
            this.fileFoundAmount.TabIndex = 17;
            this.fileFoundAmount.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 296);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Найдено интересующих файлов";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1484, 411);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.fileFoundAmount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TimePassed);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FilesAmount);
            this.Controls.Add(this.PauseSearchButton);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FileTextBox);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.FileNameTextBox);
            this.Controls.Add(this.StartDirTextBox);
            this.Controls.Add(this.StartDirButton);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File Searching";
            this.TransparencyKey = System.Drawing.SystemColors.WindowFrame;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartDirButton;
        private System.Windows.Forms.TextBox StartDirTextBox;
        private System.Windows.Forms.TextBox FileNameTextBox;
        private System.Windows.Forms.FolderBrowserDialog StartDirectoryBrowser;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox FileTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button PauseSearchButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox FilesAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TimePassed;
        private System.Windows.Forms.TextBox fileFoundAmount;
        private System.Windows.Forms.Label label7;
    }
}

